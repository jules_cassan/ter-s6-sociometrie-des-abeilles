# TER S6 - Sociométrie des abeilles

## Informations

Le fichier `ClassificationAbeilles.ipynb` contient la totalité du projet.
Le projet est composé de 5 grandes parties:

- Un modèle traditionnel
- Un modèle simple de réseau de neurones
- Un modèle plus compliqué: modèle à convolution (CNN)
- Recherche d’un classifieur efficace
- Creation d’une fake image à partir de la vidéo

Les images d'abeilles et d'alvéoles utilisées pour l'apprentissage des modèles, ainsi que la vidéo pour la dernière partie, se situent [ici](https://drive.google.com/drive/folders/1CA0-3fNW3PsQuol42veIl4-wNVEHRu8B?usp=sharing).

## Un modèle traditionel
Le but dans cette partie est de caculer la précision (accuracy) d'un modèle simple proposé par la bibliothèque `Scikit-Learn` appelé **Naïve Bayes**.\
En utilisant notre jeu de données d'apprentissage avec ce modèle nous obtenons une accuracy d'environ 85.8%.

## Un modèle simple de raiseau de neurones
Le modèle utilisé dans cette partie est constitué de 4 couches réparties comme suit:
```python
model = models.Sequential([
  layers.Flatten(input_shape=(24, 24)),
  layers.Dense(128, activation='relu'),
  layers.Dense(64, activation='relu'),
  layers.Dense(2, activation='sigmoid')
])
```
La précision de ce modéle est très faible par rapport à celle du modèle traditionnel, soit 54.2%.

## Un modèle plus compliqué: modèle à convolution (CNN)
Le modèle à covolution est plus complexe que le précédent, avec un plus grand nombre de couches qui ont des fonctionnalités différentes.
```python
model = models.Sequential([
  layers.Conv2D(filters=32, kernel_size=(3, 3), input_shape=(24, 24, 1), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2,2)),
  layers.Dropout(0.3),
  layers.Flatten(),
  layers.Dense(2, activation='sigmoid')
])
```
La précision de ce modéle est de 67.6%, qui est plus élevée que le réseau simple de neurones, mais reste tout de même faible par rapport à celle du modèle traditionnel.

## Recherche d’un classifieur efficace
Dans cette partie le but est de trouver le meilleur modèle parmis les 3 suivants en appliquant différents paramètres à chacun:
- KNeighborsClassifier
- DecisionTreeClassifier
- SVC

Le meilleur modèle parmis ces derniers, qui est aussi celui utilisé dans la partie suivante, est **KNeighborsClassifier** avec une précision de 89.4%.


## Creation d’une fake image à partir de la vidéo
Une fake-image est une image construite artificiellement afin de mettre en lumière une situation précise. Afin d'en créer une, nous devons récupérer plusieurs images de la vidéo. Une fois cela établi, il est important de supprimer les données indésirables, dans notre cas les bords noirs. Il suffit ensuite de redécouper l'image comme nous le souhaitons et d'en analyser les données. Si nous voulons créer une fake-image représentant l'ensemble des alvéoles de la ruche, il suffit de faire les étapes vues précédemment. Nous avons choisi de prendre des sous-images de taille 24 par 24 car notre modèle s'est entraîné sur ces dernières. Il suffira alors de l'utiliser sur chaque sous-image et de récupérer uniquement les alvéoles. Nous pourrons répéter ce mécanisme sur l'ensemble des images de la vidéo afin d'obtenir une image de la ruche sans aucune abeille. Dans notre situation, la vidéo est de courte durée ce qui implique un manque de données.

![Fake Image](fake_img.png)
